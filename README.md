# Description

UCoreNet is an internal library containing C# extensions methods for working within Unity.
The library currently supports Unity 2023.2.3.f1. 

The library doesn't implements automated testing and some feature are highly experimental.
No supports will be provided. Use it at your own risk.

# License

The source code is distributed under Apache License 2.0 by the IHMTEK company. 

