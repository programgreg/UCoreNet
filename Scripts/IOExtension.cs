﻿using System.Collections.Generic;

namespace UCoreNet
{
    public static class IOExtension
    {
        
        public delegate void PrintMethod(object o);
        public static PrintMethod s_printMethod = (object o) => UnityEngine.Debug.Log(o);
        public static PrintMethod s_printMethodWarning = (object o) => UnityEngine.Debug.LogWarning(o);

        /// <summary>
        /// Print the specified array of type 'T'
        /// </summary>
        public static void Print<T>(this T[,] a_doublearray)
        {
            int r = a_doublearray.GetLength(0);
            int c = a_doublearray.GetLength(1);
            string s = "";
            for (int i = 0; i < r; ++i)
            {
                s = s + "[";
                for (int j = 0; j < c; ++j)
                {
                    s += a_doublearray[i, j] + " ";
                }
                s = s.Substring(0, s.Length - 1) + "]\n\r";
            }
            s_printMethod(s);
        }

        public static string Print(this byte a_byte, bool a_bDislay = true)
        {
            string s = "";
            for (int i = 8; i >= 1; --i)
            {
                s += a_byte.GetBit(i).ToValue();
            }
            if(a_bDislay)
                s_printMethod(s);
            return s;
        }

        public static void Print(params object[] a_oToPrints)
        {
            Print((IEnumerable<object>)a_oToPrints);
        }

        public static void PrintV<T>(this T a_value)
        {
            s_printMethod(a_value);
        }

        /// <summary>
        /// Print the specified IEnumerable of type 'T'
        /// </summary>
        public static void Print<T>(this IEnumerable<T> a_enumerable)
        {
            string s = "[";
            foreach (T t in a_enumerable)
            {
                s += t + " ";
            }
            s_printMethod(s.Substring(0, s.Length - 1) + "]");
        }
    }
}