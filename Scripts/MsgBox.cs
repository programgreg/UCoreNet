﻿/**
 * \file	MsgBox.cs
 * \author	Grégoire CATTAN
 * \version 1
 * \date	01 Mars 2018
 * 
 * Initially from UnityVader
 */

using System.Collections.Generic;

/**
 * \brief	This class help to send message through the Unity scene.
 * It works like a queue. Just push message and then pope them.
 * \see StartButton
 */
public static class MsgBox {

    private static Queue<object> s_voBox = new Queue<object>();

    public static bool ContainsMsg()
    {
        return s_voBox.Count > 0;
    }

    public static void PushMsg(object o)
    {
        s_voBox.Enqueue(o);
    }

    public static T PopMsg<T>(T a_defaultValueToReturn)
    {
        if(!ContainsMsg())
        {
            return a_defaultValueToReturn;
        }

        object o = PopMsg<object>();
        if (o is T)
        {
            return (T)o;
        }

        return a_defaultValueToReturn;

    }

    public static T PopMsg<T>()
    {
        return (T) s_voBox.Dequeue();
    }
}
