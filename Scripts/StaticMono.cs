﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticMono<T> : MonoBehaviour where T : StaticMono<T> {

	static private T s_instance = null;

	public static T Get()
	{
		if(s_instance == null)
			s_instance = GameObject.FindFirstObjectByType<T>();
		return s_instance;
	}
}
