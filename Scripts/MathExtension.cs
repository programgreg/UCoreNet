﻿using System.Linq;
using UnityEngine;

namespace UCoreNet
{
   public struct Rect
    {
        public int Left { get; set; }
        public int Top { get; set; }
        public int Right { get; set; }
        public int Bottom { get; set; }

        public Rect(Rect copy) : this(copy.Left, copy.Right, copy.Top, copy.Bottom)
        {
            
        }

        public Rect(int a_iLeft, int a_iRight, int a_iTop, int a_iBottom)
        {
            Left = a_iLeft;
            Right = a_iRight;
            Top = a_iTop;
            Bottom = a_iBottom;
        }

        public int Surface()
        {
            return Width() * Height();
        }

        /// <summary>
        /// Get a percent of the width
        /// </summary>
        /// <param name="a_fPercent">Percent between 0 and 1</param>
        public int Width(float a_fPercent = 1f)
        {
            return (int) ((Right - Left) * a_fPercent);
        }

        /// <summary>
        /// Get a percent of the height
        /// </summary>
        /// <param name="a_fPercent">Percent between 0 and 1</param>
        public int Height(float a_fPercent = 1f)
        {
            IOExtension.s_printMethod(Bottom - Top);
            return (int) ((Bottom - Top) * a_fPercent);
        }
            
        public static Rect operator+(Rect a, Rect b)
        {
            Rect ret = new Rect(a);
            ret.Left += b.Left;
            ret.Right += b.Right;
            ret.Top += b.Top;
            ret.Bottom += b.Bottom;
            return ret;
        }

        public static Rect operator-(Rect a, Rect b)
        {
            Rect ret = new Rect(a);
            ret.Left -= b.Left;
            ret.Right -= b.Right;
            ret.Top -= b.Top;
            ret.Bottom -= b.Bottom;
            return ret;
        }

        public override string ToString()
        {
            return string.Format("[Rect: Left={0}, Top={1}, Right={2}, Bottom={3}, Width={4}, Height={5}, Surface={6}]", Left, Top, Right, Bottom, Width(), Height(), Surface());
        }
    }
	
    public static class MathExtension
    {
        public enum ModuloMethod
        {
            MATH,
            TRUNC,
            EUCL
        }

        public static Quaternion Inverse(this Quaternion a_quaternion)
        {
            return Quaternion.Inverse(a_quaternion);
        }

        public static Quaternion Flip(this Quaternion a_quaternion)
        {
            return Quaternion.Euler(-a_quaternion.eulerAngles);
        }

        /*!
        *  \brief return the euler angle between two quaternion
        */
        public static double EulerDistance(Quaternion a_quaternionA, Quaternion a_quaternionB)
        {
            return Mathf.Sqrt(Mathf.Pow(a_quaternionA.x - a_quaternionB.x, 2) +
                Mathf.Pow(a_quaternionA.y - a_quaternionB.y, 2) +
                Mathf.Pow(a_quaternionA.z - a_quaternionB.z, 2) +
                Mathf.Pow(a_quaternionA.w - a_quaternionB.w, 2));
        }

        /// <summary>
        /// Return the remainder and the quotient of the division of a_fInteger by a_fDivider.
        /// Three implementation (euclidian, truncation and mathematical) are provided here.
        /// Behaviour of the trunction and euclidian methods for float should be considerate carrefully.
        /// Euclidian method return default 0 when a_fInteger and a_fDivider are not of the same sign.
        /// </summary>
        /// <param name="a_iInteger">Integer to devide</param>
        /// <param name="a_iDivider">Diveder</param>
        /// <param name="a_iQuotient">Quotient (out parameter)</param>
        public static float Mod(float a_fInteger, float a_fDivider, out float a_fQuotient, ModuloMethod a_eMethod = ModuloMethod.EUCL)
        {
            
            if (a_eMethod == ModuloMethod.EUCL)
            {
                if (a_fInteger.ToSgn() != a_fDivider.ToSgn())
                {
                    a_fQuotient = 0;
                    return 0;
                }
                else
                {
                    a_fQuotient = a_fInteger >= 0 ?  DevideI(a_fInteger, a_fDivider) : 0;
                    return a_fInteger.Absi() % a_fDivider.Absi();
                }
            }
            else if (a_eMethod == ModuloMethod.MATH)
            {
                a_fQuotient = DevideI(a_fInteger, a_fDivider, a_fDivider >= 0);
                return Remainder(a_fInteger, a_fDivider, a_fQuotient);
            }

            else// if (a_eMethod == ModuloMethod.TRONC)
            {
                a_fQuotient = DevideI(a_fInteger, a_fDivider);
                return a_fInteger % a_fDivider;
            }
        }

        public static float Remainder(float a_fInteger, float a_fDivider, float a_fQuotient)
        {
            return  a_fInteger - a_fDivider * a_fQuotient;
        }

        public static int DevideI(float A, float B, bool a_bTrunc = true)
        {
            return (a_bTrunc || B > 0) ? (int) (A / B) : (int) (A / B) - 1;
        }

        public static int Absi(this float a_float)
        {
            return Absi((int)a_float);
        }

        public static int Absi(this int a_int)
        {
            return a_int * a_int.ToSgn();
        }

        /*!
        *  \brief Return the norm of the vector (a,b)
        */
        public static float Norm(float a, float b)
        {
            return Mathf.Sqrt(a * a + b * b);
        }

        public static float Absf(this float a_float)
        {
            return a_float * a_float.ToSgn();
        }

        /*!
        *  \brief Return true if the four float coordinate of the two quaternion are nearly equals two-by-two.
        * The method AreNearlyEquals(float, float, precision).
        */
        public static bool AreNearlyEquals(Quaternion a_quaternion, Quaternion a_quaternion2, float a_fPrecision)
        {
            return AreNearlyEquals(a_quaternion.x, a_quaternion2.x, a_fPrecision) &&
                AreNearlyEquals(a_quaternion.y, a_quaternion2.y, a_fPrecision) &&
                AreNearlyEquals(a_quaternion.z, a_quaternion2.z, a_fPrecision) &&
                AreNearlyEquals(a_quaternion.w, a_quaternion2.w, a_fPrecision);
        }


        /*!
	    *  \brief Return true if the absolute difference between the two floats are lower than the given precision.
        */
        public static bool AreNearlyEquals(float a_float, float a_float2, float a_fPrecision)
        {
            return Absf(a_float - a_float2) <= a_fPrecision;
        }

        /// <summary>
        ///  Return 1 if a_b is true, -1 else.
        /// <summary>
        public static float ToSgn(this float a_float)
        {
            return a_float >= 0.0f ? 1 : -1;
        }

        /// <summary>
        ///  Return 1 if a_b is true, -1 else.
        /// <summary>
        public static int ToSgn(this int a_int)
        {
            return a_int >= 0 ? 1 : -1;
        }

        /// <summary>
        ///  Return 1 if a_b is true, -1 else.
        /// <summary>
        public static int ToSgn(this bool a_b)
        {
            return a_b ? 1 : -1;
        }


        /// <summary>
        /// Return 1 if a_b is true, 0 else.
        /// <summary>
        public static int ToValue(this bool a_b)
        {
            return a_b ? 1 : 0;
        }
        /// <summary>
        ///  Return a_f is this number is between the bounds, else return the closed bound.
        /// <summary>
        public static float Bound(float a_f, float a_fMin, float a_fMax)
        {
            return a_f < a_fMin ? a_fMin : a_f > a_fMax ? a_fMax : a_f;
        }

        /// <summary>
        /// Gets a bit value into a byte
        /// </summary>
        /// <returns><c>true</c>, if the bit is equal to 1, <c>false</c> otherwise.</returns>
        /// <param name="a_byte">A byte</param>
        /// <param name="a_bNumber">Number of the bit from which we want the value</param>
        public static bool GetBit(this byte a_byte, int a_bNumber)
        {
            return (a_byte & (0x01 << (a_bNumber - 1))) > 0;
        }

        /// <summary>
        /// Sets a bit value into a byte
        /// </summary>
        /// <returns>The modified byte</returns>
        /// <param name="a_byte">A byte</param>
        /// <param name="a_bNumber">Number of the bit from which we want the value</param>
        public static byte SetBit(this byte a_byte, int a_bNumber, bool a_bValue)
        {
            if(a_bValue)
            {
                return (byte)(a_byte | 0x01 << (a_bNumber - 1));
            }
            else
            {
                return (byte)(a_byte & ~0x01 << (a_bNumber - 1));
            }
        }

        /// <summary>
        /// Convert a string representation of a binary number into a byte.
        ///string len must be equal to 8.
        /// </summary>
        public static byte ParseBinary(string a_strBinaryNumber)
        {
            byte ret = 0;
            for(int i = 0; i < 8; ++i)
            {
                if(a_strBinaryNumber[i] == '1')
                {
                    //IOExtension.s_printMethod("setting " + (8 - i));
                    ret = ret.SetBit(8 - i, true);
                }
            }
            return ret;
        }

        public static bool IsBtwn(this int a_iValue, int a_iMin, int a_iMax)
        {
            return a_iValue <= a_iMax && a_iValue >= a_iMin;
        }


        public static Vector2 GetMinXMaxY(Vector2 a_v2MinBounds, Vector2 a_v2MaxBounds)
		{
			return new Vector2(a_v2MinBounds.x, a_v2MaxBounds.y);
		}

		public static Vector2 GetMinYMaxX(Vector2 a_v2MinBounds, Vector2 a_v2MaxBounds)
		{
			return new Vector2(a_v2MaxBounds.x, a_v2MinBounds.y);
		}
    

    }
}