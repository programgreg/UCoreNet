﻿/**
 * \file	IntRange.cs
 * \author	Grégoire CATTAN
 * \version 1
 * \date	28 August 2018
 * 
 * A bounded integer with a custom inspector for Unity
 */

using System.Reflection;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UCoreNet
{
    [Serializable]
    public class BoundedInt
    {
        [SerializeField]
        private int m_iMin = 0;
        [SerializeField]
        private int m_iMax = 0;
        [SerializeField]
        private int m_iValue = 0;

        public BoundedInt(int a_iMin, int a_iMax, int a_iValue)
        {
            m_iMin = a_iMin;
            m_iMax = System.Math.Max(m_iMin, a_iMax);
            Value = a_iValue;
        }

        public int Max
        {
            get{ return m_iMax; }
        }

        public int Min
        {
            get{ return m_iMin; }
        }

        public int Value
        {
            get{ return m_iValue; }
            set
            {
                m_iValue = System.Math.Max(value, m_iMin);
                m_iValue = System.Math.Min(value, m_iMax);
            }
        }

        public float Percent
        {
            get { return (float) Value / Max; }
        }
        public override string ToString()
        {
            return string.Format("[IntRange] (" + m_iMin + ", " + m_iValue + ", " + m_iMax + ")");
        }
    }
}

#if UNITY_EDITOR
  [CustomPropertyDrawer(typeof(UCoreNet.BoundedInt))]
    public class IntRangeInspector : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position,
                                SerializedProperty property,
                                GUIContent label)
        {
            int min =  property.FindPropertyRelative("m_iMin").intValue;
            int max = property.FindPropertyRelative("m_iMax").intValue;
            int value = property.FindPropertyRelative("m_iValue").intValue;

            property.FindPropertyRelative("m_iValue").intValue = EditorGUI.IntSlider(position, label, value, min, max);

            property.serializedObject.ApplyModifiedProperties();
        }
        
    }
#endif