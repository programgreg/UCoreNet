﻿/**
 * \file	WatchDog.cs
 * \author	Grégoire CATTAN
 * \version 1
 * \date	28 Février 2018
 * 
 * Initially in UnityVader
 */

using System.Threading;


namespace UCoreNet
{
    /**
    * \brief	This class define a interface Guardian that is to be used with the WatchDog timer. WatchDog will notify
    * 			Guardian at expiration.
    */
    public interface Guardian
    {
        void OnTimeOver();
    }

    /**
    * \brief	Define a timer (watchdog)
    */
    public class WatchDog
    {

        private int m_iMs;
        private Guardian m_guardian;


        /*!
        *  \brief Constructor
        *
        *  Init a WatchDog
        *
        *  \param a_iMs : Time before expiration in ms
        *  \param a_guardian : Observer to notify when time is over
        */
        public WatchDog(int a_iMs, Guardian a_guardian)
        {
            m_iMs = a_iMs;
            m_guardian = a_guardian;
            Thread thread = new Thread(body);
            thread.Start(this);
        }

   
        private void body(object o)
        {
            WatchDog watchdog = (WatchDog)o;
            Thread.Sleep(watchdog.m_iMs);
            watchdog.m_guardian.OnTimeOver();
        }

        /*!
        *  \brief Constructor (static)
        *
        *  Init a WatchDog
        *
        *  \param a_iMs : Time before expiration in ms
        *  \param a_guardian : Observer to notify when time is over
        */
        public static void Start(int a_iMs, Guardian a_guardian)
        {
            new WatchDog(a_iMs, a_guardian);
        }
    }
}