﻿using System.Collections.Generic;
using System.IO.Ports;
using Microsoft.Win32;
using System.Threading;
using System.Management;

namespace UCoreNet
{
    public static class SystemExtension
    {
        private static Thread main = Thread.CurrentThread;

        public static Thread GetMainThread()
        {
            return main;
        }

        

        /// <summary>
        /// brief Autodetect the arduino port in the registry entries
        /// <summary>
        public static string AutodetectArduinoPort()
        {
            List<string> comports = new List<string>();
            RegistryKey rk1 = Registry.LocalMachine;
            RegistryKey rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum");
            string temp;
            foreach (string s3 in rk2.GetSubKeyNames())
            {
                RegistryKey rk3 = rk2.OpenSubKey(s3);
                foreach (string s in rk3.GetSubKeyNames())
                {
                    if (s.Contains("VID") && s.Contains("PID"))
                    {
                        RegistryKey rk4 = rk3.OpenSubKey(s);
                        foreach (string s2 in rk4.GetSubKeyNames())
                        {
                            RegistryKey rk5 = rk4.OpenSubKey(s2);
                            if ((temp = (string)rk5.GetValue("FriendlyName")) != null && temp.Contains("Arduino"))
                            {
                                RegistryKey rk6 = rk5.OpenSubKey("Device Parameters");
                                if (rk6 != null && (temp = (string)rk6.GetValue("PortName")) != null)
                                {
                                    comports.Add(temp);
                                }
                            }
                        }
                    }
                }
            }

            if (comports.Count > 0)
            {
                foreach (string s in SerialPort.GetPortNames())
                {
                    if (comports.Contains(s))
                        return s;
                }
            }

            return "COM9";
        }

        /// <summary>
        /// brief Return the system time. Used in LSL protocol for synchronizing timestamp. 
        /// see UnityTaggerWrapperRemote
        /// <summary>
        public static double Time()
        {
            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            return (System.DateTime.UtcNow - epochStart).TotalSeconds;

        }
            
        /// <summary>
        /// brief Cast 'toCast' into object, then into type A, and finally into type B.
        /// <summary>
        public static B FCast<A, B>(object toCast)
        {
            return FCast<B>(FCast<A>(toCast));
        }

        /// <summary>
        /// brief Force to cast when the pre-compiler cannot resolve the relation between the type.
        /// First, 'toCast' is automatically casted to object by calling this method. Which allow
        /// to downcast easily into type 'A'.
        /// <summary>
        public static A FCast<A>(object toCast)
        {
            return (A)toCast;
        }

        
        /// <summary>
        /// brief Other signature for the "Cast(toCast, A_type)" method where
        /// the type 'A_type' is self-determined.
        /// <summary>
        public static A SmartCast<A>(this object toCast)
        {
            return SmartCast<A>(toCast, typeof(A));
        }

        /// <summary>
        /// brief Made a custom cast of the object 'toCast' into the type 'A_type'.
        /// Supported cast are:
        /// - string to int, double, float, long, bool, Enum
        /// - string to byte (if length of string == 8, such as 00000101 => (byte) 2, else just convert into byte)
        //  - string to file content if string represents the path to an existing file
        /// - int to bool (true if int != 0)
        /// - byte to string
        /// - Default behavior is to return ((A_type) toCast).
        /// <summary>
        public static A SmartCast<A>(object toCast, System.Type A_type)
        {
            System.Type toCast_type = toCast.GetType();
            if (toCast_type == A_type && toCast_type != typeof(string))
            {
                return toCast.procust<A>();
            }
            else if (toCast_type == typeof(string))
            {
                if(A_type == typeof(string))
                {
                    
                    if(System.IO.File.Exists((string)toCast))
                    {
                        return FCast<A>(System.IO.File.ReadAllText((string)toCast));
                    }
                    else
                    {
                        return toCast.procust<A>();
                    }
                }
                if(A_type == typeof(byte))
                {
                    string s = (string) toCast;
                    if(s.Length == 8)
                    {
                        return FCast<A>(MathExtension.ParseBinary(s));
                    }
                    else
                    {
                        return FCast<A>(byte.Parse(s));
                    }
                }
                if (A_type == typeof(int))
                {
                    return FCast<A>(int.Parse((string)toCast));
                }
                else if (A_type == typeof(double))
                {
                    return FCast<A>(double.Parse((string)toCast));
                }
                else if (A_type == typeof(float))
                {
                    return FCast<A>(float.Parse((string)toCast));
                }
                else if (A_type == typeof(long))
                {
                    return FCast<A>(long.Parse((string)toCast));
                }
                else if (A_type == typeof(bool))
                {
                    return FCast<A>(bool.Parse((string)toCast));
                }
                else if (A_type.IsEnum)
                {
                    return (A)System.Enum.Parse(A_type, (string)toCast);
                }
                else if(A_type == typeof(System.DateTime))
                {
                    return FCast<A>(System.DateTime.Parse((string) toCast));
                }
                else
                {
                    return toCast.procust<A>();
                }
            }
            else if(toCast_type == typeof(int))
            {
                if(A_type == typeof(bool))
                {
                    return FCast<A>((int)toCast != 0);
                }
                else
                {
                    return toCast.procust<A>();
                }
            }
            else if(toCast_type == typeof(byte))
            {
                if(A_type == typeof(string))
                {
                    return FCast<A>(((byte)(toCast)).Print(false));
                }
                else
                {
                    return toCast.procust<A>();
                }
            }
            else
            {
                return toCast.procust<A>();
            }
        }

    }

}