﻿/**
 * \file	GLDrawerCaller.cs
 * \author	Grégoire CATTAN
 * \version 1
 * \date	02 Mars 2018
 * 
 * Initially from UnityVader
 */

using System.Net.Sockets;
using System.Threading;
using System.Net;
using System;
using System.Diagnostics;

namespace UCoreNet
{
    /**
    * \brief	private class for Connexion class. Help to connect synchronously (connect and block until connexion timeout is over)
    */
    class ConnexionParameter : Guardian
    {
        private TcpClient m_oClient;
        private string m_sAddress;
        private int m_iPort;
        private Thread m_threadThis = null;
        private int m_iTimeOut;
        private bool m_bIsConnected = true;

        private EventWaitHandle m_oEventWaitHandle = new AutoResetEvent(false);

        public ConnexionParameter(TcpClient a_oClient, string a_oAddress, int a_oPort, int a_iTimeOut)
        {
            m_oClient = a_oClient;
            m_sAddress = a_oAddress.Trim();
            m_iPort = a_oPort;
            m_iTimeOut = a_iTimeOut;
            m_threadThis = new Thread(Connect);
        }


        public void Connect()
        {
            WatchDog.Start(m_iTimeOut, this);
            try
            {
                m_oClient.Connect(IPAddress.Parse(m_sAddress), m_iPort);

            }
            catch (Exception)
            {
                IOExtension.s_printMethodWarning("Unable to connect service (Training mode? Or not running?)");
            }
            OnTimeOver();
        }


        public void OnTimeOver()
        {
            try
            {
                if (!m_oClient.Connected)
                {
                    m_bIsConnected = false;
                    m_oClient.Close();
                }
                m_oEventWaitHandle.Set();

            }
            catch (Exception)
            {
                IOExtension.s_printMethodWarning("Connexion already closed, or forcing connexion thread to end. Ignore.");
            }
        }

        public bool Get()
        {
            m_threadThis.Start();
            m_oEventWaitHandle.WaitOne();
            return m_bIsConnected;
        }
    }

    /**
    * \brief	Extension methods for TCPClient. 
    * \see TCPClientWrapper
    * \see UnityTaggerWrapperRemote
    */
    public static class Connexion
    {
        public delegate bool PingMethod();

        /*!
        *  \brief  execute 'pingMethod' and return this result after 'a_iTimeOut' ms
        *  \see UnityTaggerWrapperRemote
        */
        public static bool TimeoutMethod(int a_iTimeOut, PingMethod pingMethod)
        {
            bool ret = false;
            Thread thread = new Thread(delegate ()
                {
                    ret = pingMethod();
                });
            thread.Start();
            if (!thread.Join(a_iTimeOut))
            {
                thread.Abort();
            }
            return ret;
        }

        /*!
        *  \brief Used in TCPClientWrapper
        */
        public static bool Connect(this TcpClient a_oClient, string a_sAddress, int a_iPort, int a_iTimeOut)
        {
            ConnexionParameter cp = new ConnexionParameter(a_oClient, a_sAddress, a_iPort, a_iTimeOut);
            return cp.Get();
        }

        /*!
        *  \brief Used in TCPClientWrapper
        */
        public static Socket AcceptSocket(this TcpListener a_oTcpListener, int a_iTimeOut, int a_iPollInterval = 10)
        {
            TimeSpan timeout = TimeSpan.FromMilliseconds(a_iTimeOut);
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            while (stopWatch.Elapsed < timeout)
            {
                if (a_oTcpListener.Pending())
                    return a_oTcpListener.AcceptSocket();

                Thread.Sleep(a_iPollInterval);
            }
            return null;
        }


    }
}