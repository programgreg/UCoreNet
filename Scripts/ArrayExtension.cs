﻿using System.Linq;
using UnityEngine;

namespace UCoreNet
{
    public static class ArrayExtension
    {

        /*!
        *  \brief convert an array into a vector. Array must contains 3 cases, corresponding
        *  to the 3 coordinates of the result vector.
        */
        public static Vector3 ToVector3(this double[] array)
        {
            return new Vector3((float)array[0], (float)array[1], (float)array[2]);
        }

        /*!
        *  \brief convert a double array into a quaternion. Array must contains 4 values, corresponding
        *  to the 4 parameters of a quaternion.
        */
        public static Quaternion ToQuaternion(this double[] array)
        {
            return new Quaternion((float)array[0], (float)array[1], (float)array[2], (float)array[3]);
        }

        public static double[] ToDoubleArray(this Vector3 a_vector3)
        {
            return new double[] { a_vector3.x, a_vector3.y, a_vector3.z };
        }

        public static double[] ToDoubleArray(this Quaternion a_quat)
        {
            return new double[] { a_quat.x, a_quat.y, a_quat.z, a_quat.w };
        }

        /// <summary>
        /// Initiate the specified a_array with the default_value.
        /// </summary>
        /// <param name="a_array">An array.</param>
        /// <param name="default_value">The default value.</param>
        public static void Initiate<T>(this T[] a_array, T default_value)
        {
            int len = a_array.Length;
            for (int i = 0; i < len; ++i)
            {
                a_array[i] = default_value;
            }
        }


        /*!
        *  \brief Shuffles the elements in the list from position 'start' (included) to 'stop' (excluded).
        * if stop == -1, stop will take the length of the list (thus, the last element of the list will be included).
        */
        public static void Shuffle<T>(T[] list, int start = 0, int stop = -1)
        {
            if (stop == -1)
                stop = list.Length;
            for (int i = start; i < stop; ++i)
            {
                int randomIndex = Random.Range(i, stop);
                Switch(list, i, randomIndex);
            }
        }

        /// <summary>
        /// Switch the element at index 'a_indexA' with the element at index 'a_indexB' in the array 'a_array'.
        /// </summary>
        public static void Switch<T>(T[] a_array, int a_indexA, int a_indexB)
        {
            Switch(a_array, a_array, a_indexA, a_indexB);
        }

        /// <summary>
        /// Switch the element at index 'a_indexA' in the array 'a_vA' with the element at index 'a_indexB' in the array 'a_vB'.
        /// <summary>
        public static void Switch<T>(T[] a_vA, T[] a_vB, int a_indexA, int a_indexB)
        {
            T temp = a_vA[a_indexA];
            a_vA[a_indexA] = a_vB[a_indexB];
            a_vB[a_indexB] = temp;
        }

        /// <summary>
        /// Efficiently copy an array of type 'T' by mean of the system function.
        /// <summary>
        public static T[] ArrayCopy<T>(T[] source)
        {
            int len = source.Length;
            T[] ret = new T[len];
            System.Array.Copy(source, ret, len);
            return ret;
        }

        /// <summary>
        /// Copy the array and remove all elements 'element' in the copy array. Extension method.
        /// <summary>
        public static T[] Remove<T>(this T[] array, T element)
        {
            return array.Except(new T[] { element }).ToArray();
        }

        /// <summary>
        /// Generate a list containing the successive integers in the range [min, max[.
        /// <summary>
        public static int[] GenerateIntList(int min, int max)
        {
            int[] ret = new int[max - min];
            for (int i = min; i < max; ++i)
            {
                ret[i - min] = i;
            }
            return ret;
        }


        /// <summary>
        /// Create a copy of the list and add in the same time an element at the end of the list.
        /// This method uses appropriate system function.
        /// <summary>
        public static T[] Concat<T>(T[] list, T element)
        {
            int len = list.Length;
            T[] ret = new T[len + 1];
            System.Array.Copy(list, ret, len);
            ret[len] = element;
            return ret;
        }

        /// <summary>
        ///  extension method. Return the index of 'element' in 'a_array'.
        /// <summary>
        public static int IndexOf<T>(this T[] a_array, T element)
        {
            int len = a_array.Length;
            for (int i = 0; i < len; ++i)
            {

                if (a_array[i].Equals(element))
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// Extension method. Copy then sort the 'a_array'.
        /// <summary>
        public static T[] SortCopy<T>(this T[] a_array)
        {
            T[] ret = ArrayCopy(a_array);
            System.Array.Sort(ret);
            return ret;
        }

        public static bool IsCorrectIndex<T>(this T[] a_array, int a_iIndex)
        {
            return a_iIndex.IsBtwn(0, a_array.Length - 1);
        }

        public static T[] IncreaseStaticArray<T>(T[] a_array, int a_iNumberOfCaseInMore)
        {
            int len = a_array.Length;
            T[] ret = new T[len + a_iNumberOfCaseInMore];
            System.Array.Copy(a_array, ret, len);
            return ret;
        }

        /// <summary>
        /// Set the value in the specificied array at the given index.
        /// If the index exceed the length of the array, allocate more space to the static array.
        /// In this case, the default behavior is that the exact number of space will be allocated in order that the given index
        /// will designate the last element in the array.
        /// </summary>
        /// <param name="a_array">An array.</param>
        /// <param name="a_iIndex">The index of the element to set</param>
        /// <param name="a_value">The value of the element to set</param>
        /// <param name="a_iIncreasePercent">The number of cases to add in the array will be multiplied by this coefficient. For instance, 
        /// if a_iIndex is 11 and that the size of the array is 1, then it will be necessary to add 10 cases to the array. If a_iIncreasePercent is set to 1.1, 
        /// then we will in fact increase the space with 11 cases (10 * 1.1)</param>
        public static T[] Set<T>(this T[] a_array, int a_iIndex, T a_value, float a_iIncreasePercent = 1)
        {

            T[] ret = null;

            //a_iIndex.PrintV();
            if (!a_array.IsCorrectIndex(a_iIndex))
            {
                // ((a_iIndex - a_array.Length + 1) * a_iIncreasePercent).Print();
                int a_iNumberOfCaseToCreate = (int)((a_iIndex - a_array.Length + 1) * a_iIncreasePercent);
                ret = IncreaseStaticArray(a_array, a_iNumberOfCaseToCreate);
            }
            else
            {
                ret = a_array;
            }
            ret[a_iIndex] = a_value;
            return ret;

        }

        /// <summary>
        /// Gets elements in a_array from position 'a_iFirstIncludedIndex' included to 'a_iLastIncludedIndex' included.static
        /// 'a_iLastIncludedIndex' can be equals to -1 to designate the index of the last element of 'a_array'
        /// or -2 to designate the element before the last element of 'a_array'.
        /// <summary>
        public static T[] Gets<T>(this T[] a_array, int a_iFirstIncludedIndex, int a_iLastIncludedIndex)
        {
            if (a_iLastIncludedIndex == -1)
                a_iLastIncludedIndex = a_array.Length - 1;

            if (a_iLastIncludedIndex == -2)
                a_iLastIncludedIndex = a_array.Length - 2;

            int len = a_iLastIncludedIndex - a_iFirstIncludedIndex + 1;
            T[] ret = new T[len];
            System.Array.Copy(a_array, a_iFirstIncludedIndex, ret, 0, len);
            return ret;
        }

        /// <summary>
        /// Get an element in the array using its cylic index such -1 represents the last elements and so one.static
        /// Warning! This methods will never return an exception if 'a_iIndex' is outside the array. It will
        /// always returns and element.
        /// <summary>
        public static T Get<T>(this T[] a_array, int a_iIndex)
        {
            float quot;
            a_iIndex = (int)UCoreNet.MathExtension.Mod(a_iIndex, a_array.Length, out quot, MathExtension.ModuloMethod.MATH);
            if (a_iIndex < 0)
                a_iIndex += a_array.Length;
            return a_array[a_iIndex];
        }

        /// <summary>
        /// Returns the last elements of an array
        /// <summary>
        public static T Last<T>(this T[] a_array)
        {
            return a_array.Get(-1);
        }
    }
}