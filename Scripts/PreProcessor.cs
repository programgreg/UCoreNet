﻿/**
 * \file	PreProcessor.cs
 * \author	Grégoire CATTAN
 * \version 1
 * \date	18 Octobre 2018
 * 
 * Replace regular expression into the specified file by new ones.
 * Use the skeleton provided below to implements pre-processing instructions.
 * Basic rules is:
 * //@ [regular expression] => [to replace]
 * Specific characters in [to replace]:
 * - '.*' will be replaced by the variadic part of the regular expression if any
 * - '@' will be replaced by the founded match of the regular expression if any
 * see skeleton for examples of rules.
 * Preprocessing will happen before build time automatically with unity.
 * In edit mode use the Window/PreProcess options.
 */

///Skeleton:


// #if PreProcessor
// #region PreProcessor
// //@ public void .*() { => @ IOExtension.s_printMethod(".*");
// //@ //Genial => IOExtension.s_printMethod("Genial");

// //Pseudo-Code:

// #endregion PreProcessor
// #else //Generated
// #endif

using System.Text.RegularExpressions;
#if UNITY_EDITOR
	using UnityEditor;
	using UnityEditor.Build;
	using UnityEditor.Build.Reporting;
	using UnityEngine;
#endif

namespace UCoreNet
{
	#if UNITY_EDITOR
	[ExecuteInEditMode]
	public  class PreProcessor : EditorWindow, IPreprocessBuildWithReport  {

		[MenuItem ("Window/PreProcess")]
		public static void  ShowWindow () {
			ProcessAllCs();
    	}

		public int callbackOrder { get { return 0; } }
			
		private static void ProcessAllCs()
		{
			string[] assetPaths = AssetDatabase.GetAllAssetPaths();
			foreach(string assetPath in assetPaths)
			{
				if(assetPath.EndsWith(".cs"))
				{
					Analyse(assetPath);
				}
			}
		}
		public void OnPreprocessBuild(BuildReport report) {
			ProcessAllCs();
		}
	#else
	public  class PreProcessor {
	#endif

		public static void Analyse(string a_filePath)
		{
			//@ public * { => @ IOExtension.s_printMethod(*);
			//get source code
			string sourceCode = a_filePath.SmartCast<string>();
			//Check if source code contains instructions and that it is not the PreProcessor file itself.
			if(!sourceCode.Contains("#region PreProcessor") || a_filePath.EndsWith("PreProcessor.cs"))
			{
				//IOExtension.s_printMethod(a_filePath + " do not have preprocessing instructions...");
				return;
			}
			else
			{
				IOExtension.s_printMethod("processing " + a_filePath);
			}
			//find preprocessor instructions
			string[] splittedSourceCode = sourceCode.SplitAndGet("#endregion PreProcessor", 0).Split("//Pseudo-Code:");
			string[] allPreProcessorInstruction = splittedSourceCode[0].Split("//@").Gets(1, -1);
			//find source code to replace
			string sourceCodeToReplace = splittedSourceCode[1];
			string newSourceCode = sourceCodeToReplace.ReplaceFirst("#else //Generated", "").ReplaceLast("#endif", "");
			string[] instruction;
			string toFind;
			string toReplace;
			foreach(string ppi in allPreProcessorInstruction)//fore each instructions
			{
				instruction = ppi.Split(" => ");
				toFind = instruction[0].Trim();//regular expression to find
				toReplace = instruction[1].Trim();//replacing by this instruction
				IOExtension.s_printMethod("Replacing \"" + toFind + "\" by \"" + toReplace + "\"");
				Regex rgx = new Regex(toFind);
				string sMatch = "", sVariadic = "";
				string[] aVariadics = null;
				foreach (Match match in rgx.Matches(newSourceCode))//for each match of the regular expression
				{
					sMatch = match.Value;
					if(toFind.Contains(".*"))//find variadic element in regular expression
					{
						aVariadics = toFind.Split(".*");
						sVariadic = sMatch.Replace(aVariadics[0], "").Replace(aVariadics[1], "");
					}
					
					//replace @ by regex match and .* by the found variadic element
					newSourceCode = newSourceCode.Replace(sMatch, toReplace.Replace("@", sMatch).Replace(".*", sVariadic));

				}
			}

			//Generate new source code
			string codeToWrite = splittedSourceCode[0] + "//Pseudo-Code:" + sourceCodeToReplace +
			"#endregion PreProcessor\n#else //Generated\n#region PreProcessed" + newSourceCode + "#endregion PreProcessed\n#endif";
			System.IO.File.WriteAllText(a_filePath, codeToWrite);
			//codeToWrite.PrintV();
		}
	}

}