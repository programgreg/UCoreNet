﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCoreNet
{
	public class GLLib : StaticMono<GLLib> {

		private static Material s_mat;

		public static Canvas s_world;

		public static GUIStyle m_style = new GUIStyle();

		struct SUiText
		{
			public string text;
			public Color color;
			public bool IsOpaque;
			public UnityEngine.Rect position;

			public bool IsHorizontal;
		}

		static List<SUiText> aTexts = new List<SUiText>();

		///PRIVATE METHODS

		/*!
		*  \brief  Create a material for drawing lines
		*/
		private static void CreateLineMaterial()
		{
			if (!s_mat)
			{
				// Unity has a built-in shader that is useful for drawing
				// simple colored things.
				Shader shader = Shader.Find("Hidden/Internal-Colored");
				s_mat = new Material(shader);
				s_mat.hideFlags = HideFlags.HideAndDontSave;
				// Turn on alpha blending
				s_mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
				s_mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
				// Turn backface culling off
				s_mat.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
				// Turn off depth writes
				s_mat.SetInt("_ZWrite", 0);
			}
		}

		private static List<Vector2> GetCirclePoints(Vector2 a_v2Centrum, float a_fR, int num_segments)
		{
			List<Vector2> points = new List<Vector2>();
			float theta;
			Vector2 pointA;
			for(int i = 0; i < num_segments; ++i)
			{
				theta = 2.0f * 3.1415926f * (float) i / (float) num_segments;//get the current angle 
				pointA = new Vector2(a_fR * Mathf.Cos(theta), a_fR * Mathf.Sin(theta)) + a_v2Centrum;
				points.Add(pointA);
			}
			return points;
		}
		

		private static float Sign (Vector2 p1, Vector2 p2, Vector2 p3)
		{
			return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
		}

		//https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle#_=_
		private static bool TriangleContains (Vector2 pt, Vector2 A, Vector2 B, Vector2 C)
		{
			float d1, d2, d3;
			bool has_neg, has_pos;

			d1 = Sign(pt, A, B);
			d2 = Sign(pt, B, C);
			d3 = Sign(pt, C, A);

			has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
			has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

			return !(has_neg && has_pos);
		}

		private static bool TriangleExclude(Vector2 A, Vector2 B, Vector2 C, Vector2[] a_av2Points)
		{
			foreach(var point in a_av2Points)
			{
				if(TriangleContains(point, A, B, C) && point != A && point != B && point != C)
				{
					return false;
				}
			}
			return true;
		}

		private static bool IsEar(Vector2 A, Vector2 B, Vector2 C, Vector2[] a_av2Points)
		{
			return Mathf.Abs(Vector2.Dot(A - B, C - B)) != 1 && TriangleExclude(A, B, C, a_av2Points);
		}

		///PUBLIC METHODS

		public static void AddText(string a_text, Vector2 a_v2Position, Canvas a_canvas, bool a_bIsHorizontal = true)
		{
			AddText(a_text, a_v2Position, a_canvas, Color.black, a_bIsHorizontal);
		}

		public static void AddText(string a_text, Vector2 a_v2Position, Canvas a_canvas, Color a_color, bool a_bIsHorizontal = true, bool a_bOpaque = false)
		{
			SUiText text;
			text.text = a_text;
			text.color = a_color;
			text.IsOpaque = a_bOpaque;
			Vector2 posInWorld = a_canvas.Scale(a_v2Position);
			//HeightOffset? WidthOffset?
			text.position = new UnityEngine.Rect(posInWorld.x * Width(), (1 - posInWorld.y) * Height(), 50, 20);
			text.IsHorizontal = a_bIsHorizontal;
			aTexts.Add(text);
		}
	
		//http://csharphelper.com/blog/2014/07/triangulate-a-polygon-in-c/
		public static void FillPoly(Canvas a_canvas, Color a_color, params Vector2[] a_av2Points)
		{
			List<Vector2> points = new List<Vector2>(a_av2Points);
			int len = points.Count;
			for(int i = 0; i < len - 3; i += 1)
			{
				if(IsEar(points[i], points[i + 1], points[i + 2], a_av2Points))
				{
					DrawTriangle(points[i], points[i + 1], points[i + 2], a_canvas, a_color);
					points.RemoveAt(i + 1);
					--i;
					--len;
				}
			}
			DrawTriangle(points[0], points[1], points[2], a_canvas, a_color);
		}

		public static void DrawCircle(Canvas a_canvas, Vector2 a_v2Centrum, float a_fR, Color a_color, int num_segments = 100)
		{
			DrawPoly(a_canvas, a_color, 1, true, GetCirclePoints(a_v2Centrum, a_fR, num_segments).ToArray());
		}


		public static void FillCircle(Canvas a_canvas, Vector2 a_v2Centrum, float a_fR, Color a_color, int num_segments = 100)
		{
			FillPoly(a_canvas, a_color, GetCirclePoints(a_v2Centrum, a_fR, num_segments).ToArray());
		}

		public static void DrawCurve(Canvas a_canvas, Color a_color, params Vector3[] a_av2Points)
		{
			int len = a_av2Points.Length;
			Vector2[] curve = new Vector2[len];
			Vector2[] curveBounds = new Vector2[2 * len];
			Vector3 temp, temp2;
			curve[0] = new Vector2(a_av2Points[0].x, a_av2Points[0].y);
			for(int i = 1; i < len; ++i)
			{
				temp = a_av2Points[i - 1];
				temp2 = a_av2Points[i];
				curve[i] = new Vector2(temp.x, temp.y);
				FillTriangle(new Vector2(temp.x, temp.y + temp.z), new Vector2(temp.x, temp.y - temp.z), new Vector2(temp2.x, temp2.y - temp2.z), a_canvas, a_color);
				FillTriangle(new Vector2(temp.x, temp.y + temp.z), new Vector2(temp2.x, temp2.y + temp2.z), new Vector2(temp2.x, temp2.y - temp2.z), a_canvas, a_color);
			}
			DrawPoly(a_canvas, Color.black, 1, false, curve);
		}

		public static void DrawPoly(Canvas a_canvas, Color a_color, int a_iPas = 1, bool a_bLoop = true, params Vector2[] a_av2Points)
		{
			int len = a_av2Points.Length;
			for(int i = 0; i < len - 1; i += a_iPas)
			{
				DrawLine(a_av2Points[i], a_av2Points[i + 1], a_canvas, a_color);
			}
			if(a_bLoop)
				DrawLine(a_av2Points[len - 1], a_av2Points[0], a_canvas, a_color);
		}

		public static void FillRect(Vector2 a_v2MinBounds, Vector2 a_v2MaxBounds, Canvas a_canvas, Color a_color)
		{
			FillPoly(a_canvas, a_color, a_v2MinBounds, MathExtension.GetMinXMaxY(a_v2MinBounds, a_v2MaxBounds),
			a_v2MaxBounds, MathExtension.GetMinYMaxX(a_v2MinBounds, a_v2MaxBounds));
		}

		public static void DrawRect(Vector2 a_v2MinBounds, Vector2 a_v2MaxBounds, Canvas a_canvas, Color a_color)
		{
			DrawPoly(a_canvas, a_color, 1, true, a_v2MinBounds, MathExtension.GetMinXMaxY(a_v2MinBounds, a_v2MaxBounds),
			a_v2MaxBounds, MathExtension.GetMinYMaxX(a_v2MinBounds, a_v2MaxBounds));
		}

		public static void DrawGrid(int a_iNumGraduationX, int a_iNumGraduationY, Canvas a_canvas, Color a_color, Vector2 a_v2Margin = default(Vector2))
		{
			float t;
			for(int i = 0; i < a_iNumGraduationX; ++i)
			{
				t = a_canvas.GetXPercent(a_iNumGraduationX == 1 ? 0.5f : (float) i / (a_iNumGraduationX - 1), a_v2Margin.x);
				DrawLine(new Vector2(t, a_canvas.GetMinY()), new Vector2(t, a_canvas.GetMaxY()), a_canvas, a_color);
			}
			for(int i = 0; i < a_iNumGraduationY; ++i)
			{
				t = a_canvas.GetXPercent(a_iNumGraduationY == 1 ? 0.5f : (float) i / (a_iNumGraduationY - 1), a_v2Margin.y);
				DrawLine(new Vector2(a_canvas.GetMinX(), t), new Vector2(a_canvas.GetMaxY(), t), a_canvas, a_color);
			}
		}

		public static void DrawGridIntersections(int a_iNumGraduationX, int a_iNumGraduationY, Canvas a_canvas, Color a_color, Vector2 a_v2Margin = default(Vector2))
		{
			float x, y;
			for(int i = 0; i < a_iNumGraduationX; ++i)
			{
					x = a_canvas.GetXPercent(a_iNumGraduationX == 1 ? 0.5f : (float) i / (a_iNumGraduationX - 1), a_v2Margin.x);
				for(int j = 0; j < a_iNumGraduationY; ++j)
				{
					y = a_canvas.GetXPercent(a_iNumGraduationY == 1 ? 0.5f : (float) j / (a_iNumGraduationY - 1), a_v2Margin.y);
					DrawCircle(a_canvas, new Vector2(x, y), 1, a_color);
				}
			}
			
		}

		public static void DrawCross(Vector2 a_v2MinBounds, Vector2 a_v2MaxBounds, Canvas a_canvas, Color a_color)
		{
			DrawPoly(a_canvas, a_color, 2, false, a_v2MinBounds, a_v2MaxBounds,
			MathExtension.GetMinXMaxY(a_v2MinBounds, a_v2MaxBounds), MathExtension.GetMinYMaxX(a_v2MinBounds, a_v2MaxBounds));
		}

		public static void DrawLine(Vector2 a_v2Start, Vector2 a_v2End, Canvas a_canvas, Color a_color)
		{
			GL.Begin(GL.LINES);
			GL.Color(a_color);
			GL.Vertex(a_canvas.Scale(a_v2Start));
			GL.Vertex(a_canvas.Scale(a_v2End));
			GL.End();
		}

		public static void FillTriangle(Vector2 a_v2PointA, Vector2 a_v2PointB, Vector2 a_v2PointC, Canvas a_canvas, Color a_color)
		{
			FillPoly(a_canvas, a_color, a_v2PointA, a_v2PointB, a_v2PointC);
		}

		public static void DrawTriangle(Vector2 a_v2PointA, Vector2 a_v2PointB, Vector2 a_v2PointC, Canvas a_canvas, Color a_color)
		{
			GL.Begin(GL.TRIANGLES);
			GL.Color(a_color);
			GL.Vertex(a_canvas.Scale(a_v2PointA));
			GL.Vertex(a_canvas.Scale(a_v2PointB));
			GL.Vertex(a_canvas.Scale(a_v2PointC));
			GL.End();
		}

		public static void DrawPlus(Canvas a_canvas, Vector2 a_v2Centrum, Vector2 a_v2Extent, Color a_color)
		{
			Vector2[] points = new Vector2[12];
			points[0] = new Vector2(a_v2Centrum.x - a_v2Extent.x, a_v2Centrum.y - a_v2Extent.y);
			points[1] = new Vector2(a_v2Centrum.x - a_v2Extent.x, a_v2Centrum.y + a_v2Extent.y);
			points[2] = new Vector2(a_v2Centrum.x - a_v2Extent.y, a_v2Centrum.y + a_v2Extent.y);
			points[3] = new Vector2(a_v2Centrum.x - a_v2Extent.y, a_v2Centrum.y + a_v2Extent.x);
			points[4] = new Vector2(a_v2Centrum.x + a_v2Extent.y, a_v2Centrum.y + a_v2Extent.x);
			points[5] = new Vector2(a_v2Centrum.x + a_v2Extent.y, a_v2Centrum.y + a_v2Extent.y);
			points[6] = new Vector2(a_v2Centrum.x + a_v2Extent.x, a_v2Centrum.y + a_v2Extent.y);
			points[7] = new Vector2(a_v2Centrum.x + a_v2Extent.x, a_v2Centrum.y - a_v2Extent.y);
			points[8] = new Vector2(a_v2Centrum.x + a_v2Extent.y, a_v2Centrum.y - a_v2Extent.y);
			points[9] = new Vector2(a_v2Centrum.x + a_v2Extent.y, a_v2Centrum.y - a_v2Extent.x);
			points[10] = new Vector2(a_v2Centrum.x - a_v2Extent.y, a_v2Centrum.y - a_v2Extent.x);
		    points[11] = new Vector2(a_v2Centrum.x - a_v2Extent.y, a_v2Centrum.y - a_v2Extent.y);
			DrawPoly(a_canvas, a_color, 1, true, points);
		}

		public static void FillPlus(Canvas a_canvas, Vector2 a_v2Centrum, Vector2 a_v2Extent, Color a_color)
		{
			Vector2 dot = new Vector2(-a_v2Extent.y, a_v2Extent.x);
			FillRect(a_v2Centrum - a_v2Extent, a_v2Centrum + a_v2Extent, a_canvas, a_color);
			FillRect(a_v2Centrum - dot, a_v2Centrum + dot, a_canvas, a_color);
		}
		
		public static void InitRendering()
		{
			GL.PushMatrix();
			s_mat.SetPass(0);
			GL.LoadOrtho();
		}

		public static void CloseRendering()
		{
			GL.PopMatrix();
		}

		public static float WidthOffset(Camera cam = null)
		{
			if(cam == null)
				cam = GameObject.FindFirstObjectByType<GLLib>().gameObject.GetComponent<Camera>();
			return Screen.width * (cam.rect.x);
		}

		public static float HeightOffset(Camera cam = null)
		{
			if(cam == null)
				cam = GameObject.FindFirstObjectByType<GLLib>().gameObject.GetComponent<Camera>();
			return Screen.height * cam.rect.y;//(-1 + cam.rect.height + cam.rect.y);
		}

		public static float Height(Camera cam = null)
		{
			if(cam == null)
				cam = GameObject.FindFirstObjectByType<GLLib>().gameObject.GetComponent<Camera>();
			return Screen.height * (cam.rect.height);
		}

		public static float Width(Camera cam = null)
		{
			if(cam == null)
				cam = GameObject.FindFirstObjectByType<GLLib>().gameObject.GetComponent<Camera>();
			return Screen.width * (cam.rect.width);
		}

		public static Vector2 ConvertScreenCoordinate(Vector2 pos, Canvas a_canvas, Camera cam = null)
		{
			Vector2 extentsInWord = a_canvas.GetExtentsInWorld();
			Vector2 extents = a_canvas.GetExtents();
			Vector2 minBoundsInWorld = a_canvas.GetMinBoundsInWorld();
			Vector2 minBounds = a_canvas.GetMinBounds();
			float x = ((pos.x - WidthOffset(cam))/ (float) Width(cam) - minBoundsInWorld.x) / extentsInWord.x * extents.x + minBounds.x;
			float y = ((pos.y - HeightOffset(cam))/ (float) Height(cam)- minBoundsInWorld.y) / extentsInWord.y * extents.y + minBounds.y;
			return new Vector2(x , y);
		}

		// Use this for initialization
		void Start () {
			CreateLineMaterial();
			s_world = new Canvas(null, new Vector2(0.0f, 0.0f), new Vector2(1f, 1f),
			new Vector2(0.0f, 0.0f), new Vector2(1f, 1f));

			m_style.normal.textColor = Color.black;
			m_style.fontSize = 20;
			//m_style.normal.background = MakeTex(2, 2, Color.white);
		}


		private Texture2D MakeTex( int width, int height, Color col ){
			Color[] pix = new Color[width * height];
			for( int i = 0; i < pix.Length; ++i ){
				pix[ i ] = col;
			}
			Texture2D result = new Texture2D( width, height );
			result.SetPixels( pix );
			result.Apply();
			return result;
		}
		
		void OnGUI()
		{
			//GUI.Label(new Rect(10, 10, 100, 20), "Hello World!", style);
			//Debug.Log(aTexts.Count);
			Texture2D defaultBackground = m_style.normal.background;
			foreach(SUiText text in aTexts)
			{
				if(text.IsOpaque)
					m_style.normal.background = MakeTex(2,2, Color.white);
				m_style.normal.textColor = text.color;
				if(text.IsHorizontal)
				{			
					GUI.Label(text.position, text.text, m_style);
				}
				else
				{
					GUIUtility.RotateAroundPivot(-90, text.position.position);
					GUI.Label(text.position, text.text, m_style);
					GUIUtility.RotateAroundPivot(90, text.position.position);
				}
				if(text.IsOpaque)
					m_style.normal.background = defaultBackground;
			}
			
		}

		public static void ClearTexts()
		{
			aTexts.Clear();
		}
		// void OnPostRender()
		// {
		// 	//aTexts.Clear();
			
		// }
		
	}

}