﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCoreNet
{
	public enum eOrientation
	{
		UP, 
		DOWN, 
		LEFT, 
		RIGHT
	}

	public enum eAxisGraduation
	{
		START_AND_END,
		ALL,
		ONE_OUT_OF_TWO_WITH_OFFSET,
		NONE
	}

	public class Axis {

		private eOrientation m_eOrientation;
		private eAxisGraduation m_eAxisGraduation;
		private int m_iNumberOfGraduation;
		private Color m_color;

		public Axis(eOrientation a_eOrientation, eAxisGraduation a_eAxisGraduation, int a_iNumberOfGraduation, Color a_color)
		{
			m_eOrientation = a_eOrientation;
			m_eAxisGraduation = a_eAxisGraduation;
			m_iNumberOfGraduation = a_iNumberOfGraduation;
			m_color = a_color;
		}

		private bool IsXAxis()
		{
			return m_eOrientation == eOrientation.UP || m_eOrientation == eOrientation.DOWN;
		}

		private void DrawGraduation(Vector2 pos, float a_fValue, Canvas a_canvas)
		{
			string text = a_fValue.ToString().Substring(0, Mathf.Min(4, a_fValue.ToString().Length));

			Vector2 orth;
			Vector2 extent;
			if(IsXAxis())
			{
				orth = new Vector2(0, a_canvas.GetYExtent() * 0.005f);
				extent = new Vector2(a_canvas.GetXExtent() * 0.02f, 0);
			}
			else
			{
				orth = new Vector2(a_canvas.GetXExtent() * 0.005f, 0);	
				extent = new Vector2(a_canvas.GetXExtent() * 0.03f, -a_canvas.GetYExtent() * 0.01f);
			}

			if(a_fValue != Mathf.Infinity)
				GLLib.AddText(text, pos - extent, a_canvas);
			GLLib.DrawLine(pos + orth, pos - orth, a_canvas, m_color);		
		}

		private bool CanDrawValue(int i)
		{
			return m_eAxisGraduation == eAxisGraduation.ALL || 
					(m_eAxisGraduation == eAxisGraduation.START_AND_END && (i == 0 || i == m_iNumberOfGraduation - 1)) ||
					(m_eAxisGraduation == eAxisGraduation.ONE_OUT_OF_TWO_WITH_OFFSET && i % 2 == 1);
		}

		public void DrawAxis(Canvas a_canvas)
		{
			Vector2 start = Vector2.zero, stop = Vector2.zero;
			float min = 0;
			switch(m_eOrientation)
			{
				case eOrientation.DOWN:
					start = a_canvas.GetMinBounds();
					min = start.x;
					stop = a_canvas.GetMinYMaxX();
					break;
				case eOrientation.UP:
					start = a_canvas.GetMinXMaxY();
					min = start.x;
					stop = a_canvas.GetMaxBounds();
					break;
				case eOrientation.LEFT:
					start = a_canvas.GetMinBounds();
					min = start.y;
					stop = a_canvas.GetMinXMaxY();
					break;
				case eOrientation.RIGHT:
					
					start = a_canvas.GetMaxBounds();
					min = start.y;
					stop = a_canvas.GetMinYMaxX();
					break;
			}

			GLLib.DrawLine(start, stop, a_canvas, m_color);
			
			
			float value;
			Vector2 pos;
			for(int i = 0; i < m_iNumberOfGraduation; ++i)
			{
				pos = start + (stop - start) * i / (m_iNumberOfGraduation - 1);
				if(CanDrawValue(i))
				{
					value = (pos - start).magnitude + min;
				}
				else
				{
					value = Mathf.Infinity;
				}
				DrawGraduation(pos, value, a_canvas);
			}
		}
	}

	public class Plot : UCoreNet.Canvas {

		private Axis m_axisXDown = new Axis(eOrientation.DOWN, eAxisGraduation.ONE_OUT_OF_TWO_WITH_OFFSET, 11, Color.black);
		private Axis m_axisXUp = new Axis(eOrientation.UP, eAxisGraduation.NONE, 5, Color.black);
		private Axis m_axisYLeft = new Axis(eOrientation.LEFT, eAxisGraduation.START_AND_END, 5, Color.black);
		private Axis m_axisYRight = new Axis(eOrientation.RIGHT, eAxisGraduation.NONE, 5, Color.black);

		public Plot(Canvas a_canvasParent, Vector2 a_minBoundsInParent, Vector2 a_maxBoundsInParent, Vector2 a_minBounds, Vector2 a_maxBounds):
		base(a_canvasParent,  a_minBoundsInParent,  a_maxBoundsInParent,  a_minBounds,  a_maxBounds)
		{

		}

		public void DrawAxis()
		{
			m_axisXDown.DrawAxis(this);
			m_axisXUp.DrawAxis(this);
			m_axisYLeft.DrawAxis(this);
			m_axisYRight.DrawAxis(this);
		}

		

	}

	
}
