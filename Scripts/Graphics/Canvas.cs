﻿using UnityEngine;

namespace UCoreNet
{
	public class Canvas
	{
		protected Canvas m_oParent = null;
		protected Vector2 m_v2minBoundsInParent;
		protected Vector2 m_v2maxBoundsInParent;
		protected Vector2 m_v2minBounds;
		protected Vector2 m_v2maxBounds;

		public Canvas(Canvas a_canvasParent, Vector2 a_minBoundsInParent, Vector2 a_maxBoundsInParent, Vector2 a_minBounds, Vector2 a_maxBounds)
		{
			m_oParent = a_canvasParent;
			m_v2minBoundsInParent = a_minBoundsInParent;
			m_v2maxBoundsInParent = a_maxBoundsInParent;
			m_v2minBounds = a_minBounds;
			m_v2maxBounds = a_maxBounds;
		}

		public Canvas CreateChild(Vector2 a_minBoundsInParent, Vector2 a_maxBoundsInParent, Vector2 a_minBounds, Vector2 a_maxBounds)
		{
			return new Canvas(this, a_minBoundsInParent, a_maxBoundsInParent, a_minBounds, a_maxBounds);
		}

		public Vector2 GetMinBoundsInWorld()
		{
			return Scale(m_v2minBounds);
		}

		public bool IsIn(Vector2 a_v2Point)
		{
			return a_v2Point.x >= GetMinX() && a_v2Point.x <= GetMaxX()
				&& a_v2Point.y >= GetMinY() && a_v2Point.y <= GetMaxY();
		}

		public Vector2 GetMaxBoundsInWorld()
		{
			return Scale(m_v2maxBounds);
		}

		public Vector2 Scale(Vector2 a_v2Point)
		{
			Canvas temp = this;
			while(temp.m_oParent != null)
			{
				a_v2Point = temp.ScaleInParentFrame(a_v2Point);
				temp = (Canvas) temp.m_oParent;
			}
			return a_v2Point;
		}

		public Vector2 ScaleInParentFrame(Vector2 a_v2Point)
		{
			Vector2 ret = (a_v2Point - m_v2minBounds);
			Vector2 extentsFrom = (m_v2maxBounds - m_v2minBounds);
			Vector2 extentsTo = (m_v2maxBoundsInParent - m_v2minBoundsInParent);
			ret.x = ret.x / extentsFrom.x * extentsTo.x + m_v2minBoundsInParent.x;
			ret.y = ret.y / extentsFrom.y * extentsTo.y + m_v2minBoundsInParent.y;
			return ret;
		}

		public void DebugDraw(Color a_color)
		{
			DrawFrame(a_color);
			GLLib.DrawCross(m_v2minBounds, m_v2maxBounds, this, a_color);
		}

		public void DrawFrame(Color a_color)
		{
			GLLib.DrawRect(m_v2minBounds, m_v2maxBounds, this, a_color);
		}

		public Vector2 GetMinXMaxY()
		{
			return MathExtension.GetMinXMaxY(m_v2minBounds, m_v2maxBounds);
		}

		public Vector2 GetMinYMaxX()
		{
			return MathExtension.GetMinYMaxX(m_v2minBounds, m_v2maxBounds);
		}

		public float GetXPercent(float a_fPercent, float a_fMarginX = 0)
		{
			return (GetXExtent() - 2 * a_fMarginX) * a_fPercent + m_v2minBounds.x + a_fMarginX;
		}

		public float GetYPercent(float a_fPercent, float a_fMarginY = 0)
		{
			return (GetYExtent() - 2 * a_fMarginY) * a_fPercent + m_v2minBounds.y + a_fMarginY;
		}

		public Vector2 GetMinBounds()
		{
			return m_v2minBounds;
		}

		public Vector2 GetMaxBounds()
		{
			return m_v2maxBounds;
		}

		public float GetMinX()
		{
			return m_v2minBounds.x;
		}

		public float GetMaxX()
		{
			return m_v2maxBounds.x;
		}

		public float GetMinY()
		{
			return m_v2minBounds.y;
		}

		public float GetMaxY()
		{
			return m_v2maxBounds.y;
		}

		public float GetXExtent()
		{
			return GetMaxX() - GetMinX();
		}


		public float GetYExtent()
		{
			return GetMaxY() - GetMinY();
		}

		public Vector2 GetExtents()
		{
			return m_v2maxBounds - m_v2minBounds;
		}

		public Vector2 GetExtentsInWorld()
		{
			return GetMaxBoundsInWorld() - GetMinBoundsInWorld();
		}

		

	}

}

