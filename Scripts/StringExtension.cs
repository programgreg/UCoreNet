﻿using System.Text.RegularExpressions;
using System.Linq;

namespace UCoreNet
{
    public static class StringExtension
    {
        /// <summary>
        /// Split the specified a_string with string a_sToken.
        /// </summary>
        /// <param name="a_string">A string.</param>
        /// <param name="a_sToken">A s token.</param>
        public static string[] Split(this string a_string, string a_sToken)
        {
            return a_string.Split(new string[] { a_sToken }, System.StringSplitOptions.None);
        }

        /// <summary>
        /// For each string of an array of string, replace the substring "a_sToFind" by "a_sReplacement"
        ///  (extension method)
        /// <summary>
        public static string[] Replace(this string[] a_vStrings, string a_sToFind, string a_sReplacement)
        {
            int count = a_vStrings.Length;
            for (int i = 0; i < count; ++i)
            {
                a_vStrings[i] = a_vStrings[i].Replace(a_sToFind, a_sReplacement);
            }
            return a_vStrings;
        }

        /// <summary>
        /// brief Convert hungarion notation for code into a more understable string to be displayed.
        /// For example: the string "m_iRowCount" will become "Row Count".
        /// <summary>
        private static string HungarianToHuman(string a_sFieldName)
        {
            string ret = "";
            char[] vFieldName = a_sFieldName.ToCharArray();
            int len = vFieldName.Length;
            bool takeIt = false;
            for (int i = 0; i < len; ++i)
            {
                char c = vFieldName[i];
                if (char.IsUpper(c))
                {
                    if (!takeIt)
                    {
                        takeIt = true;
                    }
                    else
                    {
                        ret += ' ';
                    }
                }
                if (takeIt)
                {
                    ret += c;
                }
            }
            return ret;
        }

        ///-----------------------------------------------------------------------------------
        /// <summary> replace a certain section of a string with other </summary>
        /// <param name="a_str"> the given string </param>
        /// <param name="a_strToReplace"> the fragment to replace </param>
        /// <param name="a_strNew"> the new fragent </param>
        ///-----------------------------------------------------------------------------------
        public static void ReplaceOnString(ref string a_str, string a_strToReplace, string a_strNew)
        {
            if (a_str != null)
            {   
                a_str = a_str.Replace(@a_strToReplace, a_strNew);
            }
        }

        ///-----------------------------------------------------------------------------------
        /// <summary> fix special characters in a string </summary>
        /// <param name="a_strPath"> the given string </param>
        ///-----------------------------------------------------------------------------------
        public static void FixCharacters(ref string a_str, bool a_bSimple)
        {
            if (a_str != null)
            {   
                ReplaceOnString(ref a_str, "\\u00a0", " ");
                ReplaceOnString(ref a_str, "\\u00ab", a_bSimple ? "<<" : "«");
                ReplaceOnString(ref a_str, "\\u00bb", a_bSimple ? ">>" : "»");
                ReplaceOnString(ref a_str, "\\u2019", "'");
                ReplaceOnString(ref a_str, "\\u0080", a_bSimple ? "EURO" : "€");
                ReplaceOnString(ref a_str, "\\u20ac", a_bSimple ? "EURO" : "€");
                ReplaceOnString(ref a_str, "\\u20A3", a_bSimple ? "FRANC" : "₣");
                ReplaceOnString(ref a_str, "\\u0152", a_bSimple ? "CE" : "Œ");
                ReplaceOnString(ref a_str, "\\u0153", a_bSimple ? "ce" : "œ");

                ReplaceOnString(ref a_str, "\\u00c0", a_bSimple ? "A" : "À");
                ReplaceOnString(ref a_str, "\\u00e0", a_bSimple ? "a" : "à");
                ReplaceOnString(ref a_str, "\\u00c2", a_bSimple ? "A" : "Â");
                ReplaceOnString(ref a_str, "\\u00e2", a_bSimple ? "a" : "â");
                ReplaceOnString(ref a_str, "\\u00c6", a_bSimple ? "AE" : "Æ");
                ReplaceOnString(ref a_str, "\\u00e6", a_bSimple ? "ae" : "æ");
                ReplaceOnString(ref a_str, "\\u00c7", a_bSimple ? "Z" : "Ç");
                ReplaceOnString(ref a_str, "\\u00e7", a_bSimple ? "z" : "ç");
                ReplaceOnString(ref a_str, "\\u00c8", a_bSimple ? "E" : "È");
                ReplaceOnString(ref a_str, "\\u00e8", a_bSimple ? "e" : "è");
                ReplaceOnString(ref a_str, "\\u00c9", a_bSimple ? "E" : "É");
                ReplaceOnString(ref a_str, "\\u00e9", a_bSimple ? "e" : "é");
                ReplaceOnString(ref a_str, "\\u00ca", a_bSimple ? "E" : "Ê");
                ReplaceOnString(ref a_str, "\\u00ea", a_bSimple ? "e" : "ê");
                ReplaceOnString(ref a_str, "\\u00cb", a_bSimple ? "E" : "Ë");
                ReplaceOnString(ref a_str, "\\u00eb", a_bSimple ? "e" : "ë");
                ReplaceOnString(ref a_str, "\\u00ce", a_bSimple ? "I" : "Î");
                ReplaceOnString(ref a_str, "\\u00ee", a_bSimple ? "i" : "î");
                ReplaceOnString(ref a_str, "\\u00cf", a_bSimple ? "I" : "Ï");
                ReplaceOnString(ref a_str, "\\u00ef", a_bSimple ? "i" : "ï");
                ReplaceOnString(ref a_str, "\\u00d4", a_bSimple ? "O" : "Ô");
                ReplaceOnString(ref a_str, "\\u00f4", a_bSimple ? "o" : "ô");
                ReplaceOnString(ref a_str, "\\u00d9", a_bSimple ? "U" : "Ù");
                ReplaceOnString(ref a_str, "\\u00f9", a_bSimple ? "u" : "ù");
                ReplaceOnString(ref a_str, "\\u00db", a_bSimple ? "U" : "Û");
                ReplaceOnString(ref a_str, "\\u00fb", a_bSimple ? "u" : "û");
                ReplaceOnString(ref a_str, "\\u00dc", a_bSimple ? "U" : "Ü");
                ReplaceOnString(ref a_str, "\\u00fc", a_bSimple ? "u" : "ü");

                ReplaceOnString(ref a_str, "\\r\\n", "\n");
                ReplaceOnString(ref a_str, "\\n", "\n");

                if (a_bSimple)
                {
                    ReplaceOnString(ref a_str, "é", "e");
                    ReplaceOnString(ref a_str, "É", "E");
                }           
            }
        }

        /// <summary>
        /// brief Extension method almost used for getting the position of a FlashingObject in the grid.
        /// In a string, return the number in parenthesis as an int.
        /// The parameter 'number' give the number of the parenthis. Example:
        /// "(7)(5)" with number == 2 will return the number inside the second parenthesis, which is 5.
        /// <summary>
        public static int InParenthesisAsInt(this string str, int number)
        {
            return int.Parse(str.InParenthesis(number));
        }

        /// <summary>
        /// Transform all letters in the string in capital letters
        /// </summary>
        public static string ToCapital(this string str)
        {
            char first = str.ToCharArray()[0];
            first = char.ToUpper(first);
            return first + str.Substring(1, str.Length - 1);
        }

        /// <summary>
        /// brief Extension method almost used for getting the position of a FlashingObject in the grid.
        /// In a string, return the number in parenthesis as a string.
        /// The parameter 'number' give the number of the parenthis. Example:
        /// "(7)(5)" with number == 2 will return the number inside the second parenthesis, which is 5.
        /// <summary>
        public static string InParenthesis(this string str, int number)
        {
            return str.Split('(')[number].Split(')')[0];
        }

        /// <summary>
        /// Split the string with token 'a_token' and return the item at position 'a_iNumber'.
        /// 'a_iNumber' is a cyclic index such as -1 represents the last element. 
        /// <summary>
        public static string SplitAndGet(this string a_string, string a_token, int a_iNumber = -1)
        {
            return a_string.Split(a_token).Get(a_iNumber);
        }

        public static string RemoveFirstLines(this string text, int linesCount)
        {
            var lines = Regex.Split(text, "\r\n|\r|\n").Skip(linesCount);
            return string.Join(System.Environment.NewLine, lines.ToArray());
        }

        /// <summary>
        /// Reverse a string such as "truc" => "curt"
        /// <summary>
        public static string Reverse(this string a_string)
        {
            int len = a_string.Length;
            string ret = "";
            for(int i = len - 1; i >= 0; --i)
            {
                ret += a_string[i];
            }
            return ret;
        }

        //https://stackoverflow.com/questions/4940124/how-can-i-delete-the-first-n-lines-in-a-string-in-c
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        //https://stackoverflow.com/questions/14825949/replace-the-last-occurrence-of-a-word-in-a-string-c-sharp
        public static string ReplaceLast(this string Source, string Find, string Replace)
        {
                int place = Source.LastIndexOf(Find);

                if(place == -1)
                return Source;

                string result = Source.Remove(place, Find.Length).Insert(place, Replace);
                return result;
        }
    }
}