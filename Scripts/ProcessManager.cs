﻿/**
 * \file	ProcessManager.cs
 * \author	Grégoire CATTAN
 * \author  Bastien MAUREILLE
 * \version 3
 * \date	31 August 2018
 * 
 * Initially in UnityVader
 * Added in version 3:
 * -IProcessContainer
 * -IProcess
 */

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace UCoreNet
{

    /**
    * \brief    A container for process.
    * Basically just a list of process with common method such as WaitForAll or HaveExited, 
    * which return true if all the included processes have exited.
    */
    public class IProcessContainer {
        private List<IProcess> m_aIProcess = null;

        public IProcessContainer()
        {
            m_aIProcess = new List<IProcess>();
        }

        public static IProcessContainer operator+(IProcessContainer a_processContainer, Process a_process)
        {
            a_processContainer.m_aIProcess.Add(new IProcess(a_process));
            return a_processContainer;
        }

        public void WaitForAll()
        {
            foreach(IProcess ip in m_aIProcess)
                ip.WaitForExit();
        }

        public void KillAll()
        {
            foreach(IProcess ip in m_aIProcess)
                ip.Kill();
        }

        public bool HaveExited()
        {
            foreach(IProcess ip in m_aIProcess)
            {
                if(!ip.HasExited())
                    return false;
            }
            return true;
        }

        public int Count()
        {
            return m_aIProcess.Count;
        }
    }

    /**
    * \brief	A wrapper class for IProcess.
    * IProcess convert usual callback into readable boolean.
    * It also fix some issue with the OnExit method by directly looking for the name of the process
    * into the system map.
    */
    public class IProcess {
        private Process m_process = null;
        private bool m_bHasExited = false;
        private bool m_bInputError = false;
        private bool m_bIsDisposed = false;
        private bool m_bOutputDataReceived =  false;

        private string m_bName = "";
        public IProcess(Process a_process)
        {
            m_process = a_process;
            m_process.Disposed += (sender, e) => {m_bIsDisposed = true;};
            m_process.ErrorDataReceived += (sender, e) => {m_bInputError = true;};
            m_process.Exited +=  (sender, e) => {m_bHasExited = true;};
            m_process.OutputDataReceived += (sender, e) => {m_bOutputDataReceived = true;};
        }

        public bool IsRunning()
        {
            return ProcessManager.IsRunning(Name());
        }

        public bool HasExited()
        {   
            if(m_bHasExited)
                return true;
            if(!IsRunning())
            {
                m_bHasExited = true;
                return true;
            }
            return  false;
        }

        public bool IsThereInputError()
        {
            return m_bInputError;
        }

        public bool WasOutputDataReceived()
        {
            return m_bOutputDataReceived;
        }

        public bool IsDisposed()
        {
            return m_bIsDisposed;
        }

        public string Name()
        {
            if(m_bName == "")
            {
                try
                {
                    m_bName = m_process.ProcessName;
                } 
                catch
                {
                    m_bHasExited = true;
                } 
            }
            return m_bName;
        }

        /**
        * \brief	Use carefully. The beahviour of this method when extern
        * people close the ran process was not tested.
        */
        public void Kill()
        {
            try
            {
                m_process.Kill();
            } 
            catch
            {
                ProcessManager.Kill(Name());
            }
        }

        public void Dispose()
        {
            m_process.Dispose();
        }

        public void WaitForExit()
        {
            m_process.WaitForExit();
        }

    }

    /**
    * \brief	This class help to manager process through C# programming language.
    */
    public static class ProcessManager
    {

        public enum ConsoleCtrlEvent
        {
            CTRL_C = 0,
            CTRL_BREAK = 1,
            CTRL_CLOSE = 2,
            CTRL_LOGOFF = 5,
            CTRL_SHUTDOWN = 6
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool GenerateConsoleCtrlEvent(ConsoleCtrlEvent sigevent, int dwProcessGroup);



        public static Process GetProcessByName(string a_sName)
        {
            Process[] allWithName = Process.GetProcessesByName(a_sName);
            return allWithName.Length == 0 ? null : allWithName[0];
        }

        public static Process GetProcessByNameContaining(string a_sPartOfName, System.DateTime a_dateTimePosteriorOrEqualTo = default(System.DateTime))
        {
            Process[] allProcesses = Process.GetProcesses();
        
            foreach (Process process in allProcesses)
            {
                try
                {
                    if (process.ProcessName.Contains(a_sPartOfName) && (a_dateTimePosteriorOrEqualTo != default(System.DateTime) || process.StartTime > a_dateTimePosteriorOrEqualTo))
                    {

                        return process;
                    }
                }
                catch (System.InvalidOperationException)
                {

                }
            }
            return null;
        }

        private static string GetDefaultDirectory(string a_sPath)
        {
            string[] parts = a_sPath.Split('\\');
            int fileName = parts[parts.Length - 1].Length;
            return a_sPath.Substring(0, a_sPath.Length - fileName);
        }

        public static Process StartProcess(string a_sPath, bool a_bHidden = true, string a_sParameters = null, string a_sWorkingDirectory = null, bool a_bRedirectStandardOutput = false, IProcessContainer a_processContainer = null)
        {
            ProcessStartInfo psi = new ProcessStartInfo(a_sPath, a_sParameters);
            //IOExtension.s_printMethod(GetDefaultDirectory(a_sPath));
            psi.WorkingDirectory = a_sWorkingDirectory == null ? GetDefaultDirectory(a_sPath) : a_sWorkingDirectory;
            psi.RedirectStandardOutput = a_bRedirectStandardOutput;
            psi.UseShellExecute = !a_bRedirectStandardOutput;
            psi.WindowStyle = a_bHidden ? ProcessWindowStyle.Hidden : ProcessWindowStyle.Normal;
            Process ret = Process.Start(psi);
            if(a_processContainer != null)
                a_processContainer += ret;
            return ret;
        }


        /*!
        *  \brief This method executes a command on the shell. 
        * Optional parameters are:
        * - 'a_bWaitForExit', if you want to wait for the end of the execution of the given command
        * - 'a_bHidden', to execute the shell in hidden mode
        * \return the process created or null if 'a_bWaitForExit' is set to true
        */
        public static Process ExecuteCommand(string command, bool a_bWaitForExit = true, bool a_bHidden = true, IProcessContainer a_processContainer = null)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);//Use "/k" for leaving the windows opened. "/c" close the prompt after execution (even if an error occured).
            processInfo.CreateNoWindow = a_bHidden;
            processInfo.UseShellExecute = !a_bHidden;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = a_bHidden;
            processInfo.RedirectStandardOutput = a_bHidden;

            process = Process.Start(processInfo);
            if (a_bWaitForExit)
            {
                process.WaitForExit();

                // *** Read the streams ***
                // Warning: This approach can lead to deadlocks, see Edit #2
                if (a_bHidden)
                {
                    string output = process.StandardOutput.ReadToEnd();
                    string error = process.StandardError.ReadToEnd();

                    exitCode = process.ExitCode;

                    IOExtension.s_printMethod("output>>" + (System.String.IsNullOrEmpty(output) ? "(none)" : output));
                    IOExtension.s_printMethod("error>>" + (System.String.IsNullOrEmpty(error) ? "(none)" : error));
                    IOExtension.s_printMethod("ExitCode: " + exitCode.ToString());
                }

                process.Close();
                return null;
            }
            if(a_processContainer != null)
                a_processContainer += process;
            return process;
        }

        public static void ExecuteLater(System.Action a_action, int a_iDelayMS)
        {
            System.Threading.Tasks.Task.Delay(a_iDelayMS).ContinueWith(t=> a_action());
        }

        public static void KillWindowContaining(string a_sName)
        {
            StartProcess("powershell", true, "Get-Process | Where-Object { $_.MainWindowTitle -like '*" + a_sName + "*' } | Stop-Process");
        }


        public static void InterruptWindowContaining(string a_sName)
        {
            Process process = StartProcess("powershell", true, "(Get-Process | Where-Object { $_.MainWindowTitle -like '*" + a_sName + "*' }).Id", a_bRedirectStandardOutput: true);
            string output = process.StandardOutput.ReadToEnd();
            IOExtension.s_printMethod("Interupt powershell subprocess output : \n" + output);

            int pid = int.Parse(output);
            Process.GetProcessById(pid).StandardInput.Close();
            //GenerateConsoleCtrlEvent(ConsoleCtrlEvent.CTRL_C, pid);
            //GenerateConsoleCtrlEvent(ConsoleCtrlEvent.CTRL_CLOSE, pid);
        }


        public static bool IsRunning(string a_sPartOfProcessName)
        {
            return GetProcessByNameContaining(a_sPartOfProcessName) != null;
        }

        public static void Kill(string a_sProcessName)
        {
            Process process;
            if ((process = GetProcessByNameContaining(a_sProcessName)) != null)
            {
                try
                {
                    process.Kill();
                }
                catch (System.InvalidOperationException)
                {
                    IOExtension.s_printMethodWarning("unable to kill " + a_sProcessName);
                }
            }
        }
    }

}