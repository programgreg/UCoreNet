﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Reflection.Emit;

namespace UCoreNet
{
    public class Children
    {
        private IEnumerable<Type> m_aChildren = null;

        public static Children Get<T>()
        {
            return new Children(ReflectionExtension.Children<T>());
        }

        private Children(IEnumerable<Type> a_aChildren)
        {
            m_aChildren = a_aChildren;
        }

        public Children(object a_object)
        {
            m_aChildren = a_object.Children();
        }

        public List<object> Create()
        {
            List<object> res = new List<object>();
            foreach (Type t in m_aChildren)
            {
                Activator.CreateInstance(t);
            }
            return res;
        }
            
    }

    public static class ReflectionExtension
    {
        /// <summary>
        /// Extension method for a FieldInfo, which return the first attribute of type 'T' of this FieldInfo.
        /// If there is no attribute, return default(T).
        /// </summary>
        /// <returns>The all binding flags.</returns>
        public static T GetAttribute<T>(this FieldInfo a_fieldInfo)
        {
            object[] all = a_fieldInfo.GetCustomAttributes(typeof(T), true);
            if (all.Length == 0)
            {
                return default(T);
            }
            else
            {
                return (T)all[0];
            }
        }

        /// <summary>
        /// Gets all binding flags.
        /// </summary>
        /// <returns>The all binding flags.</returns>
        public static BindingFlags GetAllBindingFlags()
        {
            System.Array array = System.Enum.GetValues(typeof(BindingFlags));
            BindingFlags ret = BindingFlags.Default;
            foreach (var v in array)
                ret = ret | (BindingFlags)v;
            return ret;
        }

        /// <summary>
        /// Gets all field properties of an object. Method is recursive in parents.
        /// </summary>
        /// <returns>The value</returns>
        public static List<FieldInfo> GetAllFields(this object a_object)
        {
            Type t = a_object.GetType();
            List<FieldInfo> allFields = new List<FieldInfo>();
            while(t != null)
            {
                allFields.AddRange(t.GetFields(GetAllBindingFlags()));
                t = t.BaseType;    
            }
            return allFields;
        }

        /// <summary>
        /// Gets the field property of an object. Method is recursive in parents.
        /// Returns null is no field found.
        /// </summary>
        /// <returns>The value</returns>
        public static FieldInfo GetField(this object a_object, string a_sFieldName)
        {
            Type t = a_object.GetType();
            FieldInfo fi = null;
            while(t != null && fi == null)
            {
                fi = t.GetField(a_sFieldName, GetAllBindingFlags());
                t = t.BaseType;    
            }
            
            return fi;
        }

        /// <summary>
        /// Gets the value of a field into an object (search is recursive in parent)
        /// Return default(T) if field was not found.
        /// </summary>
        /// <returns>The value</returns>
        /// <param name="a_object">The object containing the field</param>
        /// <param name="a_sFieldName">The name of the field</param>
        /// <typeparam name="T">The type of the value</typeparam>
        public static T GetFieldValue<T>(this object a_object, string a_sFieldName)
        {
            FieldInfo fi = a_object.GetField(a_sFieldName);
            if(fi != null)
            {
                return (T)fi.GetValue(a_object);
            }
            return default(T);
        }


        /// <summary>
        /// Sets the value of the field in the given object (search is recursive in parent)
        /// Returns true if the field was set.
        /// </summary>
        /// <param name="a_object">A object.</param>
        /// <param name="a_sFieldName">The name of the field</param>
        /// <param name="a_oValue">The new value of the field</param>
        public static bool SetFieldValue(this object a_object, string a_sFieldName, object a_oValue)
        {
            FieldInfo fi = a_object.GetField(a_sFieldName);
            if(fi != null && a_oValue.GetType().IsSubclassOf(fi.FieldType))
            {
                fi.SetValue(a_object, a_oValue);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets a method inside an object
        /// </summary>
        /// <returns>The MethodInfo object pointing on the method</returns>
        /// <param name="a_object">An object.</param>
        /// <param name="a_sMethodName">The method name</param>
        public static MethodInfo GetMethod(this object a_object, string a_sMethodName)
        {
            return a_object.GetType().GetMethod(a_sMethodName, GetAllBindingFlags());
        }

        /// <summary>
        /// Invokes the method on the given object.
        /// </summary>
        /// <returns>The result of the method</returns>
        /// <param name="a_object">The object which should invoke the method</param>
        /// <param name="a_sMethodName">The name of the method</param></param>
        /// <param name="a_oParameters">The parameter of the method</param>
        public static object InvokeMethod(this object a_object, string a_sMethodName, params object[] a_oParameters)
        {
            if (a_oParameters == null)
                a_oParameters = new object[]{ };
            return  a_object.GetMethod(a_sMethodName).Invoke(a_object, a_oParameters);
        }

        /// <summary>
        /// Execute the given method only if the given object is not null.
        /// </summary>
        /// <returns>The result of the method or the default result passed as argument</returns>
        /// <param name="a_object">An object.</param>
        /// <param name="a_sMethod">The name of the method</param></param>
        /// <param name="a_oDefaultResult">A default result to return if the object is null</param>
        /// <param name="a_oParameters">The parameters of the method</param>
        /// <typeparam name="T">The exepcted type of the result of the method</typeparam>
        public static T IfNotNull <T>(object a_object, string a_sMethod, object a_oDefaultResult = null, params object[] a_oParameters) where T: class
        {
            return a_object == null ? (T)a_oDefaultResult : (T)a_object.InvokeMethod(a_sMethod, a_oParameters);
        }

        /// <summary>
        /// Get the type of the class inheriting the given type 'T'
        /// </summary>
        /// <typeparam name="T">List all children of type 'T'</typeparam>
        public static IEnumerable<Type> Children<T>()
        {
            IEnumerable<Type> _allTypes = Assembly.GetAssembly(typeof(T)).GetTypes().Concat(
                                          Assembly.GetCallingAssembly().GetTypes().Concat(
                                              Assembly.GetExecutingAssembly().GetTypes().Concat(
                                                  IfNotNull<Type[]>(Assembly.GetEntryAssembly(), "GetTypes", new Type[]{ })
                                              )
                                          )
                                      );
            return _allTypes.Where(myType => myType.IsSubclassOf(typeof(T)));
        }

        /// <summary>
        /// Get the type of the class inheriting the type of the given object.
        /// </summary>
        /// <param name="a_object">A object.</param>
        public static IEnumerable<Type> Children(this object a_object)
        {
            return Assembly.GetAssembly(a_object.GetType()).GetTypes().Where(myType => myType.IsSubclassOf(a_object.GetType()));
        }

        /// <summary>
        /// Return a pointer to the method 'a_sMethodName' int the object 'a_object'
        /// </summary>
        public static IntPtr GetMethodPtr(this object a_object, string a_sMethodName)
        {
            return a_object.GetMethod(a_sMethodName).GetPtr();
        }

        /// <summary>
        /// Return a int pointer to a methodinfo
        /// </summary>
        public static IntPtr GetPtr(this MethodInfo a_iMethodInfo)
        {
            return a_iMethodInfo.MethodHandle.Value;
        }


        /// <summary>
        /// Get IL instructions of method as a byte array
        /// </summary>
        public static byte[] GetBodyAsByteArray(this MethodInfo a_iMethodInfo)
        {
            return a_iMethodInfo.GetMethodBody().GetILAsByteArray();
        }

        public static void PrintAllFields(this object a_object)
        {
            List<FieldInfo> l = a_object.GetAllFields();
            foreach(var v in l)
            {
                (v.Name + " = " + v.GetValue(a_object)).PrintV();
            }
        }

        public static A procust<A>(this object a_object)
        {
            try
            {
                return SystemExtension.FCast<A>(a_object);
            }
            catch(Exception)
            {
               // e.PrintV();
                A newInstance = (A) Activator.CreateInstance(typeof(A));
                List<FieldInfo> allFieldOfB = a_object.GetAllFields();
               // FieldInfo aFieldOfA = null;
                foreach(FieldInfo fi in allFieldOfB)
                {
                    newInstance.SetFieldValue(fi.Name, fi.GetValue(a_object));
                }
                return newInstance;
            }
        }
    }
}
