﻿using System.Collections;
using NUnit.Framework;
using UCoreNet;
using UnityEngine;
using UnityEngine.TestTools;

public class ArrayExtensionTest
{

    [Test]
    public void Initiate()
    {
        int[] test = new int[] { 1, 3, 7, 0 };
        test.Initiate(-1);
        for (int i = 0; i < 4; ++i)
        {
            Assert.AreEqual(test[i], -1);
        }
    }

    [Test]
    public void Switch()
    {
        int[] first = new int[] { 1, 1, 1, 2 };
        int[] second = new int[] { 2, 2, 2, 2 };
        ArrayExtension.Switch(first, 0, 3);
        Assert.IsTrue(first[0] == 2 && first[3] == 1);
        ArrayExtension.Switch(first, second, 1, 2);
        Assert.IsTrue(first[1] == 2 && second[2] == 1);
    }

    [Test]
    public void ArrayCopy()
    {
        int[] init = new int[] { 1, 2, 3 };
        int[] copy = ArrayExtension.ArrayCopy(init);
        // Check than init and copy are the same
        for (int i = 0; i < init.Length; ++i)
        {
            Assert.AreEqual(init[i], copy[i]);
        }
        // Check that this is not a shallow copy
        init[0] = -1;
        Assert.AreEqual(copy[0], 1);
    }

    [Test]
    public void Remove()
    {
        int[] init = new int[] { 1, 2, 3 };
        int[] copy = init.Remove(2);
        Assert.AreEqual(copy.Length, 2);
        Assert.AreEqual(copy[0], 1);
        Assert.AreEqual(copy[1], 3);
        // Assert initial array was not changed
        Assert.AreEqual(init.Length, 3);
    }

    [Test]
    public void GenerateIntList()
    {
        int[] list = ArrayExtension.GenerateIntList(0, 5);
        Assert.AreEqual(list.Length, 5);
        for (int i = 0; i < 5; ++i)
            Assert.AreEqual(list[i], i);
    }

    [Test]
    public void Concat()
    {
        int[] a = new int[] { 0, 1 };
        int b = 2;
        int[] ret = ArrayExtension.Concat(a, b);
        // Check a were not affected
        Assert.AreEqual(a.Length, 2);
        // Check ret is the concatenation of a and b
        Assert.AreEqual(ret.Length, 3);
        for (int i = 0; i < 3; ++i)
            Assert.AreEqual(ret[i], i);
    }

    [Test]
    public void IndexOf()
    {
        int[] list = new int[] { 3, 2, 1 };
        Assert.AreEqual(list.IndexOf(3), 0);
    }

    [Test]
    public void SortCopy()
    {
        int[] list = new int[] { 3, 2, 1 };
        int[] cpy = list.SortCopy();
        // Check the initial list were left unsorted
        Assert.AreEqual(list[0], 3);
        // Check copy list is sorted
        Assert.AreEqual(cpy[0], 1);
        Assert.AreEqual(cpy[1], 2);
        Assert.AreEqual(cpy[2], 3);
    }

    [Test]
    public void IsCorrectIndex()
    {
        int[] list = new int[] { 3, 2, 1 };
        Assert.AreEqual(list.IsCorrectIndex(10), false);
        Assert.AreEqual(list.IsCorrectIndex(-1), false);
        Assert.AreEqual(list.IsCorrectIndex(3), false);
        Assert.AreEqual(list.IsCorrectIndex(0), true);
        Assert.AreEqual(list.IsCorrectIndex(1), true);
        Assert.AreEqual(list.IsCorrectIndex(2), true);
    }

    [Test]
    public void IncreaseStaticArray()
    {
        int[] list = new int[] { 3, 2, 1 };
        int[] copy = ArrayExtension.IncreaseStaticArray(list, 2);
        Assert.AreEqual(copy.Length, list.Length + 2);
    }

    [Test]
    public void Set()
    {
        int[] list = new int[] { 3, 2, 1 };
        list.Set(0, 4);
        Assert.AreEqual(list[0], 4);
    }

    [Test]
    public void Gets()
    {
        int[] list = new int[] { 3, 2, 1 };
        int[] sub = list.Gets(1, -1);
        Assert.AreEqual(sub.Length, 2);
        Assert.AreEqual(sub[0], 2);
        Assert.AreEqual(sub[1], 1);
    }

    [Test]
    public void Get()
    {
        int[] list = new int[] { 3, 2, 1 };
        Assert.AreEqual(list.Get(0), 3);
        Assert.AreEqual(list.Get(3), 3);
        Assert.AreEqual(list.Get(-2), 2);
    }

    [Test]
    public void Last()
    {
        int[] init = new int[] { 1, 2, 3 };
        int last = init.Last();
        Assert.AreEqual(last, 3);
    }
}

